last updated: 2023/02/01

disclosure: Mulch is a DivestOS project

Chromium History
================
Version			Date		Security Bugfix Count	Release Notes
109.0.5414.118		2023/01/24	6			https://chromereleases.googleblog.com/2023/01/stable-channel-update-for-desktop_24.html
109.0.5414.86		2023/01/10	17			https://chromereleases.googleblog.com/2023/01/stable-channel-update-for-desktop.html
108.0.5359.128		2022/12/12	8			https://chromereleases.googleblog.com/2022/12/stable-channel-update-for-desktop_13.html
108.0.5359.79		2022/12/02	1, 1 wild		https://chromereleases.googleblog.com/2022/12/stable-channel-update-for-desktop.html
108.0.5359.61		2022/11/29	28			https://chromereleases.googleblog.com/2022/11/stable-channel-update-for-desktop_29.html
107.0.5304.141		2022/11/24	1, 1 wild		https://chromereleases.googleblog.com/2022/11/stable-channel-update-for-desktop_24.html
107.0.5304.105		2022/11/08	10			https://chromereleases.googleblog.com/2022/11/stable-channel-update-for-desktop.html
107.0.5304.91		2022/10/27	1, 1 wild		https://chromereleases.googleblog.com/2022/10/stable-channel-update-for-desktop_27.html
107.0.5304.54		2022/10/25	14			https://chromereleases.googleblog.com/2022/10/stable-channel-update-for-desktop_25.html
106.0.5249.126		2022/10/13				https://chromium.googlesource.com/chromium/src/+log/106.0.5249.118..106.0.5249.126?pretty=fuller&n=10000
106.0.5249.118		2022/10/11	6			https://chromereleases.googleblog.com/2022/10/stable-channel-update-for-desktop_11.html
106.0.5249.79		2022/09/30	3			https://chromereleases.googleblog.com/2022/09/stable-channel-update-for-desktop_30.html
106.0.5249.65		2022/09/27	24			https://chromereleases.googleblog.com/2022/09/stable-channel-update-for-desktop_27.html
105.0.5195.136		2022/09/14	11			https://chromereleases.googleblog.com/2022/09/stable-channel-update-for-desktop_14.html
105.0.5195.124		2022/09/13				https://chromium.googlesource.com/chromium/src/+log/105.0.5195.79..105.0.5195.124?pretty=fuller&n=10000
105.0.5195.79		2022/09/05				https://chromium.googlesource.com/chromium/src/+log/105.0.5195.77..105.0.5195.79?pretty=fuller&n=10000
105.0.5195.77		2022/09/02	1,  1 wild		https://chromereleases.googleblog.com/2022/09/stable-channel-update-for-desktop.html
105.0.5195.68		2022/08/30	24, 1 critical		https://chromereleases.googleblog.com/2022/08/stable-channel-update-for-desktop_30.html
104.0.5112.97		2022/08/16	12, 1 wild, 1 critical	https://chromereleases.googleblog.com/2022/08/stable-channel-update-for-desktop_16.html
104.0.5112.69		2022/08/02	27			https://chromereleases.googleblog.com/2022/08/stable-channel-update-for-desktop.html
103.0.5060.129		2022/07/19				https://chromereleases.googleblog.com/2022/07/chrome-for-android-update_01510389319.html
103.0.5060.71		2022/07/04	3,  1 wild		https://chromereleases.googleblog.com/2022/07/chrome-for-android-update.html
103.0.5060.70		2022/06/29				https://chromium.googlesource.com/chromium/src/+log/103.0.5060.53..103.0.5060.70?pretty=fuller&n=10000
103.0.5060.53		2022/06/21	13, 1 critical		https://chromereleases.googleblog.com/2022/06/stable-channel-update-for-desktop_21.html
102.0.5005.125		2022/06/13	7			https://chromereleases.googleblog.com/2022/06/stable-channel-update-for-desktop.html
102.0.5005.99		2022/06/07				https://chromereleases.googleblog.com/2022/06/chrome-for-android-update.html
102.0.5005.78		2022/05/28				https://chromereleases.googleblog.com/2022/05/chrome-for-android-update_28.html
102.0.5005.59		2022/05/24	32, 1 critical		https://chromereleases.googleblog.com/2022/05/stable-channel-update-for-desktop_24.html
101.0.4951.61		2022/05/09	13			https://chromereleases.googleblog.com/2022/05/chrome-for-android-update.html
101.0.4951.41		2022/04/26	29			https://chromereleases.googleblog.com/2022/04/stable-channel-update-for-desktop_26.html
100.0.4896.127		2022/04/14	2,  1 wild		https://chromereleases.googleblog.com/2022/04/stable-channel-update-for-desktop_14.html
100.0.4896.88		2022/04/11	12			https://chromereleases.googleblog.com/2022/04/stable-channel-update-for-desktop_11.html
100.0.4896.79		2022/04/04	1			https://chromereleases.googleblog.com/2022/04/stable-channel-update-for-desktop.html
100.0.4896.58		2022/03/29	28			https://chromereleases.googleblog.com/2022/03/stable-channel-update-for-desktop_29.html
99.0.4844.88		2022/03/25	1,  1 wild		https://chromereleases.googleblog.com/2022/03/stable-channel-update-for-desktop_25.html
99.0.4844.73		2022/03/15	11, 1 critical		https://chromereleases.googleblog.com/2022/03/stable-channel-update-for-desktop_15.html
99.0.4844.58		2022/03/03				https://chromium.googlesource.com/chromium/src/+log/99.0.4844.48..99.0.4844.58?pretty=fuller&n=10000
99.0.4844.48		2022/03/01	28			https://chromereleases.googleblog.com/2022/03/stable-channel-update-for-desktop.html
98.0.4758.101		2022/02/14	11, 1 wild		https://chromereleases.googleblog.com/2022/02/stable-channel-update-for-desktop_14.html
98.0.4758.87		2022/02/01	27			https://chromereleases.googleblog.com/2022/02/stable-channel-update-for-desktop.html
97.0.4692.98		2022/01/19	26, 1 critical		https://chromereleases.googleblog.com/2022/01/stable-channel-update-for-desktop_19.html
97.0.4692.87		2022/01/11				https://chromium.googlesource.com/chromium/src/+log/97.0.4692.70..97.0.4692.87?pretty=fuller&n=10000
97.0.4692.70		2022/01/04	38, 1 critical		https://chromereleases.googleblog.com/2022/01/stable-channel-update-for-desktop.html
96.0.4664.104		2021/12/13	5,  1 wild, 1 critical	https://chromereleases.googleblog.com/2021/12/stable-channel-update-for-desktop_13.html
96.0.4664.92		2021/12/06	22			https://chromereleases.googleblog.com/2021/12/stable-channel-update-for-desktop.html
96.0.4664.45		2021/11/15	25			https://chromereleases.googleblog.com/2021/11/stable-channel-update-for-desktop.html
95.0.4638.74		2021/11/02	9,  2 wild		https://chromereleases.googleblog.com/2021/10/stable-channel-update-for-desktop_28.html
95.0.4638.50		2021/10/19	19			https://chromereleases.googleblog.com/2021/10/stable-channel-update-for-desktop_19.html
94.0.4606.85		2021/10/11				https://chromium.googlesource.com/chromium/src/+log/94.0.4606.80..94.0.4606.85?pretty=fuller&n=10000
94.0.4606.80		2021/10/07	4			https://chromereleases.googleblog.com/2021/10/stable-channel-update-for-desktop.html
94.0.4606.71		2021/09/30	4,  2 wild		https://chromereleases.googleblog.com/2021/09/stable-channel-update-for-desktop_30.html
94.0.4606.61		2021/09/23	1,  1 wild		https://chromereleases.googleblog.com/2021/09/stable-channel-update-for-desktop_24.html
94.0.4606.50		2021/09/21	18			https://chromereleases.googleblog.com/2021/09/stable-channel-update-for-desktop_21.html
93.0.4577.82		2021/09/13	10, 2 wild		https://chromereleases.googleblog.com/2021/09/stable-channel-update-for-desktop.html
93.0.4577.62		2021/08/31	27			https://chromereleases.googleblog.com/2021/08/stable-channel-update-for-desktop_31.html


OS History
==========
note: the cumulative count of known security vulnerabilities (ksv) is noted if behind
Version		Chromium	Bromite		Vanadium	Mulch		LineageOS	CalyxOS		/e/OS
109.0.5414.118	2023/01/24	23ksv		2023/01/24	2023/01/25	23ksv		2023/01/27	23ksv
109.0.5414.86	2023/01/10			2023/01/10	2023/01/10			2023/01/15
108.0.5359.128	2022/12/12	2022/12/19	2022/12/12	2022/12/12	2022/12/20	2022/12/15	2023/01/03
108.0.5359.79	2022/12/02	2022/12/03	2022/12/02	2022/12/02	2022/12/06	2022/12/02
108.0.5359.61	2022/11/29	2022/12/02	2022/11/29	2022/11/29			2022/12/01
107.0.5304.141	2022/11/24			2022/11/24	2022/11/24			2022/11/25
107.0.5304.105	2022/11/08	2022/11/19	2022/11/08	2022/11/08	2022/11/12	2022/11/09
107.0.5304.91	2022/10/27	2022/11/16	2022/10/27	2022/10/28	2022/11/01	2022/11/08
107.0.5304.54	2022/10/25			2022/10/26	2022/10/25
106.0.5249.126	2022/10/13	2022/10/31	2022/10/13	2022/10/14	2022/10/17	2022/10/14
106.0.5249.118	2022/10/11			2022/10/11	2022/10/11			2022/10/12
106.0.5249.79	2022/09/30			2022/09/30	2022/10/01	2022/10/05	2022/10/07
106.0.5249.65	2022/09/27	2022/10/20	2022/09/27	2022/09/27			2022/09/29
105.0.5195.136	2022/09/14	2022/09/28	2022/09/14	2022/09/15	2022/09/16	2022/09/15
105.0.5195.124	2022/09/13			2022/09/13	2022/09/13			2022/09/13
105.0.5195.79	2022/09/05			2022/09/05	2022/09/05	2022/09/10	2022/09/06
105.0.5195.77	2022/09/02			2022/09/03	2022/09/02			2022/09/05
105.0.5195.68	2022/08/30			2022/08/30	2022/08/30	2022/09/01	2022/09/02
104.0.5112.97	2022/08/16	2022/09/18	2022/08/16	2022/08/16	2022/08/21	2022/08/31
104.0.5112.69	2022/08/02	2022/08/08	2022/08/02	2022/08/02
103.0.5060.129	2022/07/19	2022/07/25	2022/07/20	2022/07/19
103.0.5060.71	2022/07/04	2022/07/12	2022/07/04	2022/07/04	2022/07/12	2022/07/04
103.0.5060.70	2022/06/29			2022/06/30	2022/06/29			2022/06/30
103.0.5060.53	2022/06/21			2022/06/21	2022/06/22			2022/06/24
102.0.5005.125	2022/06/13			2022/06/13	2022/06/14			2022/06/16
102.0.5005.99	2022/06/07	2022/06/11	2022/06/07	2022/06/07			2022/06/09
102.0.5005.78	2022/05/28	2022/06/05	2022/05/30	2022/05/30	2022/06/03	2022/06/03
102.0.5005.59	2022/05/24	2022/05/28	2022/05/24	2022/05/24
101.0.4951.61	2022/05/09	2022/05/15	2022/05/10	2022/05/10	2022/05/13
101.0.4951.41	2022/04/26	2022/05/02	2022/04/29	2022/04/27
100.0.4896.127	2022/04/14	2022/04/18	2022/04/16	2022/04/16	2022/04/19	2022/04/15
100.0.4896.88	2022/04/11	2022/04/15	2022/04/13	2022/04/12
100.0.4896.79	2022/04/04	2022/04/11	2022/04/04	2022/04/04
100.0.4896.58	2022/03/29	2022/03/29	2022/03/29	2022/03/30	2022/04/03			2022/04/01
99.0.4844.88	2022/03/25			2022/03/25	2022/03/26			2022/03/27
99.0.4844.73	2022/03/15	2022/03/19	2022/03/16	2022/03/18	2022/03/19	2022/03/17	2022/03/22
99.0.4844.58	2022/03/03	2022/03/12	2022/03/04	2022/03/05			2022/03/12
99.0.4844.48	2022/03/01	2022/03/07	2022/03/02	2022/03/02					2022/03/08
98.0.4758.101	2022/02/14	2022/02/21	2022/02/15	2022/02/15	2022/02/16			2022/02/24
98.0.4758.87	2022/02/01			2022/02/01	2022/02/01			2022/02/08
97.0.4692.98	2022/01/19	2022/02/17	2022/01/20	2022/01/21	2022/01/21	2022/02/03	2022/02/18
97.0.4692.87	2022/01/11			2022/01/14	2022/01/19
97.0.4692.70	2022/01/04			2022/01/05	2022/01/05
96.0.4664.183			2022/02/05									2022/02/16
96.0.4664.104	2021/12/13	2022/02/04	2021/12/14	2021/12/14	2021/12/15
96.0.4664.92	2021/12/06			2021/12/08	2021/12/07
96.0.4664.45	2021/11/15	2021/12/04	2021/11/15	2021/11/15
95.0.4638.74	2021/11/02	2021/11/07	2021/11/04	2021/11/02
95.0.4638.50	2021/10/19			2021/10/19	2021/10/19	2021/10/21	2021/11/07
94.0.4606.85	2021/10/11	2021/10/16	2021/10/12					2021/10/13	20221/12/06
94.0.4606.80	2021/10/07			2021/10/08	2021/10/08
94.0.4606.71	2021/09/30			2021/09/30	2021/10/02	2021/10/03


OS Current Versions
===================
- latest stable		109.0.5414.118				https://chromiumdash.appspot.com/releases?platform=Android

Standalone:
- GrapheneOS Vanadium	109.0.5414.118				https://github.com/GrapheneOS/Vanadium/commits/13
- DivestOS/Mulch	109.0.5414.118				https://gitlab.com/divested-mobile/mulch
- CarbonROM, †2, 12	96.0.4664.111, 12 is not shipping yet?	https://github.com/CarbonROM/android_external_chromium-webview/commits/cr-10.0
- OmniROM, †2, 11	96.0.4664.54				https://gitlab.com/omnirom/android_packages_apps_OmniChromium/-/commits/staging
- Ungoogled Chromium	99.0.4844.51				https://github.com/ungoogled-software/ungoogled-chromium-android/commits/master

Lineage related:
- LineageOS, †2		108.0.5359.128				https://review.lineageos.org/q/project:LineageOS%252Fandroid_external_chromium-webview_prebuilt_arm64
- MoKee, †2, 12.1	108.0.5359.128, LineageOS prebuilts	https://github.com/MoKee/android_external_chromium-webview_prebuilt_arm64
- OmniROM, †2, 13	108.0.5359.109, LineageOS prebuilts	https://gitlab.com/omnirom/android_packages_apps_OmniChromium/-/commits/android-13.0
- ArrowOS, †1, 11	96.0.4664.104, LineageOS prebuilts	https://github.com/ArrowOS/android_external_chromium-webview
- CarbonROM, †2, 11	tracks LineageOS/master			https://github.com/CarbonROM/android/blob/cr-9.0/carbon-aosp.xml
- crDroid, †2, 11/12.1	tracks LineageOS/master			https://github.com/crdroidandroid/android/blob/12.1/snippets/lineage.xml
- iodéOS		tracks LineageOS/master			https://gitlab.com/iode/os/public/manifests/android/-/blob/v3-staging/snippets/lineage.xml
- Replicant		???

Bromite related:
- Bromite		108.0.5359.156				https://github.com/bromite/bromite/commits/master
- CalyxOS		109.0.5414.117				https://review.calyxos.org/q/project:CalyxOS/platform_prebuilts_calyx_chromium_arm64
- /e/OS			108.0.5359.156, downstream Bromite	https://gitlab.e.foundation/e/os/browser/-/commits/master
- Vostok		105.0.5195.41, Bromite prebuilts	https://gitlab.com/vostok/external_chromium-webview/-/commits/master
- lin-microG		108.0.5359.156, Bromite prebuilts	https://github.com/MSe1969/android_external_bromite-webview

AOSP related:
- AOSP, 7.1.2		52.7243.100				https://android.googlesource.com/platform/external/chromium-webview/+log/android-7.1.2_r39
- AOSP, 8.1		61.0.3163.98				https://android.googlesource.com/platform/external/chromium-webview/+log/android-security-8.1.0_r93
- AOSP, 9		66.0.3359.158				https://android.googlesource.com/platform/external/chromium-webview/+log/android-security-9.0.0_r76
- AOSP, 10		74.0.3729.186				https://android.googlesource.com/platform/external/chromium-webview/+log/android-security-10.0.0_r73
- AOSP, 11		83.0.4103.120				https://android.googlesource.com/platform/external/chromium-webview/+log/android-security-11.0.0_r62
- AOSP, 12		91.0.4472.114				https://android.googlesource.com/platform/external/chromium-webview/+log/android-security-12.0.0_r42
- AOSP, 12.1		95.0.4638.74				https://android.googlesource.com/platform/external/chromium-webview/+log/android-12.1.0_r27
- AOSP, 13		101.0.4951.61				https://android.googlesource.com/platform/external/chromium-webview/+log/android-13.0.0_r16
- ArrowOS, †1, 12.1	AOSP prebuilts				https://github.com/ArrowOS/android_manifest/blob/arrow-12.1/default.xml
- DotOS, †1, 11/12.1	AOSP prebuilts				https://github.com/DotOS/manifest
- HavocOS, †1, 11/12.1	AOSP prebuilts				https://github.com/Havoc-OS/android_manifest
- Paranoid, †2, 12?	AOSP prebuilts				https://github.com/AOSPA/manifest
- PixelExp, †1, 11/12.1	AOSP prebuilts				https://github.com/PixelExperience/manifest
- ProtonAOSP, †1, 12.1	AOSP prebuilts				https://github.com/ProtonAOSP/android_manifest
- StagOS, †1, 11/12.1	AOSP prebuilts				https://github.com/StagOS/manifest


Notes:
- †1: Available in a Google Apps variant with Chrome/Google WebView included.
- †2: Users are likely to install Google Apps if not included.
- This document tracks versions used in the context that Google Apps are *not* used.
- If you want your OS added/removed, please ask.
- Not all security issues impact the Android version
- This tracks dates the version was committed to a repository, *not* the date it was pushed out to users or shipped in a system image.
  - DivestOS pushes updates same day via an F-Droid repository.
  - GrapheneOS ships them out rapidly within a day or two via their Seamless Updater.
  - CalyxOS primarily pushes out updates via the monthly system updates.
    Users can opt-in to the CalyxOS testing F-Droid repository for quicker updates.
  - Bromite pushes updates via an F-Droid repository, but is sometimes a few days behind the version available via GitHub Releases.
    Users can choose to use FFUpdater to get GitHub release notifications.
  - Users with Google Apps would have Google Chrome+Google WebView pushed via Play Store updates, notably however Google does staged rollouts.
  - /e/OS appears to hold releases and only ships them once per stable release.
- Bromite has diverged from shipping stable release versions and often ships pre-release or LTS versions instead.
  These do not align cleanly with the table here, and makes it hard to determine how many security issues actually impact it.
  examples: https://github.com/bromite/bromite/discussions/2421

There is a Firefox equivalent of this table here: https://divestos.org/misc/ffa-dates.txt

See something wrong? Open an issue or merge request:
- https://gitlab.com/Divested-Mobile/DivestOS-Website/-/blob/master/misc/ch-dates.txt
- https://github.com/Divested-Mobile/DivestOS-Website/blob/master/misc/ch-dates.txt
